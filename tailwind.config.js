module.exports = {
  theme: {
    inset: {
      '0': '0',
      '4rem': '4rem'
    },
    extend: {
      colors: {
        primary: '#F2EBE3',
        secondary: '#DECBB7',
        tertiary: '#2A1F13',
        accent: '#2CA58D',
        accent2: '#08605F'
      }
    },
    borderWidth: {
      DEFAULT: '1px',
      '0': '0',
      '2': '2px',
      '3': '3px',
      '4': '4px',
      '6': '6px',
      'lg': '1rem',
    }
  },
  variants: {},
  plugins: [],
}