# TODO

## Add
* One or more of: Java, Kotlin, Scala, Go, C++
* Data structures, algorithm design, problem-solving, complexity analysis, secure coding best * practices and remediation
* Designing, building and maintaining large-scale, high-performance systems and frameworks
* Object-oriented concepts and systems design patterns
* Analyzing and troubleshooting distributed systems
* Tackling problems of load, scale, and optimizations of complex large-scale deployments
* Modern server scaling technologies on Linux (e.g., async, non-blocking I/O, multithreading* )

* Being experienced in:

* Architecture and design of cloud-scale distributed systems
* IP networking, network analysis, performance and application issues
* Event-driven architecture and technologies
* Message-Brokers(e.g. RabbitMQ, ActiveMQ)and Event-Streaming( e.g. Kafka) solutions
* RDBMS (PostgreSQL, MySQL) or distributed NoSQL Database systems (e.g. MongoDB, Redis, Memcached)
* Automated CI/CD solutions
* High availability and business continuity principles
* Cloud technologies (preferably GCP)
* Containerization and container orchestration technologies (preferably K8s)
* One or more of: Python, Shell, Perl