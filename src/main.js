import Vue from 'vue'
import App from './App.vue'
import VueI18n from 'vue-i18n'
import en from './locales/en.json';
import es from './locales/es.json';

import './assets/style.css'

const messages = {
  en,
  es
};

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'en', // set locale
  messages, // set locale messages
})


Vue.config.productionTip = false

new Vue({
  el: '#app',
  i18n,
  render: h => h(App)
})